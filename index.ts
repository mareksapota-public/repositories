// MIT Expat License

// Copyright 2020-2025 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import {
  VisibilityLevel,
  WeeklyContainerRebuild,
  group,
  project,
} from "./repo-manager";
import * as repoManager from "./repo-manager";

const publicGroup = group({
  name: "Marek Sapota",
  slug: "mareksapota-public",
  description: "Marek Sapota projects",
  projects: [
    project({
      name: "Repositories",
      description: "GitLab repository orchestration",
    }),
  ],
  groups: [
    group({
      name: "Configuration",
      description: "Configuration for various tools and software.",
      projects: [
        project({
          name: "Dotfiles",
          description: "Configuration files for various software",
          buildsAccessLevel: repoManager.AccessLevel.DISABLED,
        }),
      ],
    }),
    group({
      name: "FUGL",
      description: "FU GNU/Linux",
      projects: [
        project({
          name: "Conditional Execute",
          description:
            "Conditionally execute a command, like periodic software updates",
        }),
      ],
      groups: [
        group({
          name: "Sideload",
          description:
            "Tools for sideloading software onto GNU/Linux distros " +
            "from outisde of the standard package repositories.",
          projects: [
            project({
              name: "VCS",
              description: "Sideload software from VCS releases",
            }),
            project({
              name: "Nix",
              description: "Sideload software using the Nix package manager",
            }),
            project({
              name: "Ruby",
              description: "Sideload software using Ruby Gems",
            }),
          ],
        }),
        group({
          name: "Bootstrap",
          description: "Tools for bootstrapping OS installations.",
          projects: [
            project({
              name: "Armbian",
              description: "Bootstrap an Armbian installation",
            }),
          ],
        }),
      ],
    }),
    group({
      name: "Kotlin",
      description: "Kotlin libraries and utilities.",
      projects: [
        project({
          name: "Boilerplate",
          description: "Gradle configuration generator and base project setup",
        }),
        project({
          name: "GPIO",
          description: "GPIO access library",
        }),
        project({
          name: "Readable Time Parser",
          description:
            "Parse human readable strings and convert them " +
            "to kotlin.time.Duration",
        }),
        project({
          name: "Shell Command",
          description: "Subprocess execution library",
        }),
      ],
    }),
    group({
      name: "VSCode",
      description: "VSCode extensions.",
      projects: [
        project({
          name: "MRUTabs",
          description: "VSCode extension that keeps tabs in MRU order",
          packagesEnabled: true,
        }),
      ],
    }),
    group({
      name: "VCS",
      description: "Tools and utilities for Version Control Systems.",
      projects: [
        project({
          name: "Self Update",
          description: "Helper scripts for updating repos",
        }),
      ],
    }),
    group({
      name: "Containers",
      description: "OCI container images for various tools",
      groups: [
        group({
          name: "openSUSE",
          description: "openSUSE based containers",
          projects: [
            project({
              name: "Slowroll",
              description:
                "Container for openSUSE Slowroll\nhttps://en.opensuse.org/openSUSE:Slowroll",
              containerRegistryAccessLevel: repoManager.AccessLevel.ENABLED,
              pipelineSchedules: [WeeklyContainerRebuild({})],
            }),
            project({
              name: "Systemd",
              description: "Systemd enabled openSUSE container",
              containerRegistryAccessLevel: repoManager.AccessLevel.ENABLED,
              pipelineSchedules: [WeeklyContainerRebuild({})],
            }),
          ],
        }),
        group({
          name: "Alpine",
          description: "Alpine Linux based containers",
          projects: [
            project({
              name: "OpenRC",
              description: "OpenRC enabled Alpine Linux container",
              containerRegistryAccessLevel: repoManager.AccessLevel.ENABLED,
              pipelineSchedules: [WeeklyContainerRebuild({})],
            }),
          ],
        }),
      ],
      projects: [
        project({
          name: "VSCode integration tests",
          description:
            "Container for running VSCode integration tests in GitLab CI.",
          containerRegistryAccessLevel: repoManager.AccessLevel.ENABLED,
          pipelineSchedules: [WeeklyContainerRebuild({})],
          releasesAccessLevel: repoManager.AccessLevel.ENABLED,
        }),
        project({
          name: "pip-tools",
          description:
            "Containers for running pip-tools based on various versions of Python.",
          containerRegistryAccessLevel: repoManager.AccessLevel.ENABLED,
          pipelineSchedules: [WeeklyContainerRebuild({})],
        }),
        project({
          name: "obs-studio",
          description: "Container for running obs-studio in distrobox.",
          containerRegistryAccessLevel: repoManager.AccessLevel.ENABLED,
          pipelineSchedules: [WeeklyContainerRebuild({})],
        }),
      ],
    }),
    group({
      name: "Mirrors",
      description: "Mirrors of various projects",
      projects: [
        project({
          name: "nwa",
          description: "Mirror of https://github.com/B1NARY-GR0UP/nwa",
          issuesEnabled: false,
          containerRegistryAccessLevel: repoManager.AccessLevel.ENABLED,
          protectedBranches: ["gitlab-ci"],
        }),
      ],
    }),
  ],
});

repoManager.parse({
  defaultVisibilityLevel: VisibilityLevel.PUBLIC,
  groups: [publicGroup],
});

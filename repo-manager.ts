// MIT Expat License

// Copyright 2020-2025 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import * as gitlab from "@pulumi/gitlab";

export enum AccessLevel {
  DISABLED = "disabled",
  PRIVATE = "private",
  ENABLED = "enabled",
}

export enum VisibilityLevel {
  PUBLIC = "public",
  PRIVATE = "private",
}

export enum CronDay {
  MONDAY = 1,
  TUESDAY = 2,
  WEDNESDAY = 3,
  THURSDAY = 4,
  FRIDAY = 5,
  SATURDAY = 6,
  SUNDAY = 0,
}

function pathFromDisplayName(displayName: string): string {
  return displayName.toLowerCase().replace(/ /g, "-");
}

export type CronScheduleParameters = {
  minute?: number; // 0-59
  hour?: number; // 0-23
  dayOfMonth?: number; // 1-31
  month?: number; // 1-12
  dayOfWeek?: CronDay;
};

export class CronSchedule {
  readonly minute: number | undefined;
  readonly hour: number | undefined;
  readonly dayOfMonth: number | undefined;
  readonly month: number | undefined;
  readonly dayOfWeek: number | undefined;

  constructor({
    minute,
    hour,
    dayOfMonth,
    month,
    dayOfWeek,
  }: CronScheduleParameters) {
    if (minute !== undefined && (minute < 0 || minute > 59)) {
      throw new Error("minute outside of allowed range");
    }
    this.minute = minute;
    if (hour !== undefined && (hour < 0 || hour > 23)) {
      throw new Error("hour outside of allowed range");
    }
    this.hour = hour;
    if (dayOfMonth !== undefined && (dayOfMonth < 1 || dayOfMonth > 31)) {
      throw new Error("dayOfMonth outside of allowed range");
    }
    this.dayOfMonth = dayOfMonth;
    if (month !== undefined && (month < 0 || month >= 60)) {
      throw new Error("month outside of allowed range");
    }
    this.month = month;
    if (dayOfWeek !== undefined && (dayOfWeek < 0 || dayOfWeek >= 60)) {
      throw new Error("dayOfWeek outside of allowed range");
    }
    this.dayOfWeek = dayOfWeek;
  }

  public toString(): string {
    return [this.minute, this.hour, this.dayOfMonth, this.month, this.dayOfWeek]
      .map((value) => (value === undefined ? "*" : value))
      .join(" ");
  }
}

export type PipelineScheduleDefinition = {
  cron: CronSchedule;
  name: string;
  description: string;
  ref: string;
  cronTimezone: string;
};

export type WeeklyContainerRebuildParameters = {
  hour?: number;
  dayOfWeek?: CronDay;
};

export function WeeklyContainerRebuild({
  hour = 20,
  dayOfWeek = CronDay.WEDNESDAY,
}: WeeklyContainerRebuildParameters): PipelineScheduleDefinition {
  return {
    cron: new CronSchedule({
      hour: hour,
      dayOfWeek: dayOfWeek,
    }),
    name: "weekly-rebuild",
    description: "Weekly image rebuild",
    ref: DEFAULT_BRANCH,
    cronTimezone: "America/Los_Angeles",
  };
}

export enum ContainerExpirationPolicyCadence {
  DAILY = "1d",
  WEEKLY = "7d",
  BIWEEKLY = "14d",
  MONTHLY = "1month",
  QUARTERLY = "3month",
}

export enum ContainerExpirationPolicyOlderThan {
  DAY = "1d",
  WEEK = "7d",
  TWO_WEEKS = "14d",
  MONTH = "30d",
  TWO_MONTHS = "60d",
  QUARTER = "90d",
}

export type ContainerExpirationPolicyParameters = {
  enabled?: boolean;
  cadence?: ContainerExpirationPolicyCadence;
  keepN?: number;
  olderThan?: ContainerExpirationPolicyOlderThan;
};

export type ContainerExpirationPolicyDefinition = {
  enabled: boolean;
  cadence: string;
  keepN: number;
  olderThan: string;
  nameRegexDelete: string;
};

export function ContainerExpirationPolicy({
  enabled = true,
  cadence = ContainerExpirationPolicyCadence.DAILY,
  keepN = 10,
  olderThan = ContainerExpirationPolicyOlderThan.QUARTER,
}: ContainerExpirationPolicyParameters): ContainerExpirationPolicyDefinition {
  return {
    enabled: enabled,
    cadence: cadence,
    keepN: keepN,
    olderThan: olderThan,
    nameRegexDelete: ".*",
  };
}

export type ProjectDefinition = {
  name: string;
  description: string;
  visibilityLevel: VisibilityLevel | undefined;
  containerRegistryAccessLevel: AccessLevel;
  containerExpirationPolicy?: ContainerExpirationPolicyDefinition;
  defaultBranch: string | undefined;
  pipelineSchedules: PipelineScheduleDefinition[];
  issuesEnabled: boolean;
  releasesAccessLevel: AccessLevel;
  buildsAccessLevel: AccessLevel;
  protectedBranches: string[];
  packagesEnabled: boolean;
};

export type ProjectParameters = {
  name: string;
  description: string;
  visibilityLevel?: VisibilityLevel;
  containerRegistryAccessLevel?: AccessLevel;
  containerExpirationPolicy?: ContainerExpirationPolicyDefinition;
  defaultBranch?: string;
  pipelineSchedules?: PipelineScheduleDefinition[];
  issuesEnabled?: boolean;
  releasesAccessLevel?: AccessLevel;
  buildsAccessLevel?: AccessLevel;
  protectedBranches?: string[];
  packagesEnabled?: boolean;
};

export function project({
  name,
  description,
  visibilityLevel = undefined,
  containerRegistryAccessLevel = AccessLevel.DISABLED,
  containerExpirationPolicy = undefined,
  defaultBranch = undefined,
  pipelineSchedules = [],
  issuesEnabled = true,
  releasesAccessLevel = AccessLevel.DISABLED,
  buildsAccessLevel = AccessLevel.ENABLED,
  protectedBranches = [],
  packagesEnabled = false,
}: ProjectParameters): ProjectDefinition {
  return {
    name: name,
    description: description,
    visibilityLevel: visibilityLevel,
    containerRegistryAccessLevel: containerRegistryAccessLevel,
    containerExpirationPolicy: containerExpirationPolicy,
    defaultBranch: defaultBranch,
    pipelineSchedules: pipelineSchedules,
    issuesEnabled: issuesEnabled,
    releasesAccessLevel: releasesAccessLevel,
    buildsAccessLevel: buildsAccessLevel,
    protectedBranches: protectedBranches,
    packagesEnabled: packagesEnabled,
  };
}

export type GroupDefinition = {
  name: string; // Display name
  slug: string; // URL slug
  description: string;
  visibilityLevel?: VisibilityLevel;
  projects: Array<ProjectDefinition>;
  groups: Array<GroupDefinition>;
};

export type GroupParameters = {
  name: string;
  slug?: string; // Defaults to name
  description: string;
  visibilityLevel?: VisibilityLevel;
  projects?: Array<ProjectDefinition>;
  groups?: Array<GroupDefinition>;
};

export function group({
  name,
  slug = undefined,
  description,
  visibilityLevel = undefined,
  projects = [],
  groups = [],
}: GroupParameters): GroupDefinition {
  const slugValue = slug === undefined ? pathFromDisplayName(name) : slug;

  return {
    name: name,
    slug: slugValue,
    description: description,
    visibilityLevel: visibilityLevel,
    projects: projects,
    groups: groups,
  };
}

function parseProject(
  projectDefinition: ProjectDefinition,
  resourceNamePrefix: string,
  group: gitlab.Group,
  defaultVisibilityLevel: VisibilityLevel,
  defaultDefaultBranch: string,
): void {
  const path = pathFromDisplayName(projectDefinition.name);
  const resourceName = `${resourceNamePrefix}/${path}`;
  const visibilityLevel =
    projectDefinition.visibilityLevel === undefined
      ? defaultVisibilityLevel
      : projectDefinition.visibilityLevel;
  const defaultBranch =
    projectDefinition.defaultBranch === undefined
      ? defaultDefaultBranch
      : projectDefinition.defaultBranch;
  let projectArgs: gitlab.ProjectArgs = {
    path: path,
    name: projectDefinition.name,
    description: projectDefinition.description,
    namespaceId: group.id.apply(Number),

    visibilityLevel: visibilityLevel,

    mirror: false,
    initializeWithReadme: false,

    defaultBranch: defaultBranch,
    mergeMethod: "ff",
    squashOption: "always",
    mergeRequestsEnabled: true,
    onlyAllowMergeIfPipelineSucceeds: true,
    onlyAllowMergeIfAllDiscussionsAreResolved: true,
    resolveOutdatedDiffDiscussions: true,
    removeSourceBranchAfterMerge: true,

    mergeCommitTemplate:
      "Merge branch '%{source_branch}' into '%{target_branch}'\n" +
      "\n" +
      "%{title}\n" +
      "\n" +
      "%{description}\n" +
      "\n" +
      "%{issues}\n" +
      "\n" +
      "See merge request %{reference}\n" +
      "%{approved_by}\n" +
      "Merged-by: %{merged_by}",

    squashCommitTemplate:
      "%{title}\n" +
      "\n" +
      "%{description}\n" +
      "\n" +
      "%{issues}\n" +
      "\n" +
      "See merge request %{reference}\n" +
      "%{approved_by}\n" +
      "Merged-by: %{merged_by}",

    issuesEnabled: projectDefinition.issuesEnabled,
    autocloseReferencedIssues: true,

    issuesTemplate: "",

    sharedRunnersEnabled: true,

    requestAccessEnabled: false,
    issuesAccessLevel: projectDefinition.issuesEnabled
      ? AccessLevel.ENABLED
      : AccessLevel.DISABLED,
    repositoryAccessLevel: AccessLevel.ENABLED,
    mergeRequestsAccessLevel: AccessLevel.ENABLED,
    forkingAccessLevel: AccessLevel.ENABLED,
    buildsAccessLevel: projectDefinition.buildsAccessLevel,
    analyticsAccessLevel: AccessLevel.ENABLED,
    securityAndComplianceAccessLevel: AccessLevel.PRIVATE,
    pagesAccessLevel: AccessLevel.DISABLED,
    modelExperimentsAccessLevel: AccessLevel.DISABLED,
    modelRegistryAccessLevel: AccessLevel.DISABLED,
    monitorAccessLevel: AccessLevel.DISABLED,
    environmentsAccessLevel: AccessLevel.DISABLED,
    featureFlagsAccessLevel: AccessLevel.DISABLED,
    infrastructureAccessLevel: AccessLevel.DISABLED,
    releasesAccessLevel: projectDefinition.releasesAccessLevel,

    archived: false,
    containerRegistryAccessLevel:
      projectDefinition.containerRegistryAccessLevel,
    containerExpirationPolicy: projectDefinition.containerExpirationPolicy,
    lfsEnabled: false,
    snippetsEnabled: false,
    wikiEnabled: false,
    packagesEnabled: projectDefinition.packagesEnabled,
  };

  if (
    projectDefinition.containerRegistryAccessLevel != AccessLevel.DISABLED &&
    projectDefinition.containerExpirationPolicy == undefined
  ) {
    projectArgs = {
      ...projectArgs,
      containerExpirationPolicy: ContainerExpirationPolicy({
        cadence: ContainerExpirationPolicyCadence.WEEKLY,
        olderThan: ContainerExpirationPolicyOlderThan.MONTH,
      }),
    };
  }
  if (visibilityLevel == VisibilityLevel.PUBLIC) {
    projectArgs = {
      ...projectArgs,
      // Not available on the free Gitlab tier.
      // mergeRequestsTemplate:
      //     "[title]\n" +
      //     "\n" +
      //     "## Summary\n" +
      //     "\n" +
      //     "[summary of changes]\n" +
      //     "\n" +
      //     "Closes [URLs of related issues]\n" +
      //     "\n" +
      //     "## Test plan\n" +
      //     "\n" +
      //     "[how were the changes tested?]",
    };
  }

  const project = new gitlab.Project(`project:${resourceName}`, projectArgs, {
    // Buggy API, causes updates on every apply
    // ignoreChanges: ["containerExpirationPolicy.nameRegexDelete"],
  });

  const branchProtectionArgs: gitlab.BranchProtectionArgs = {
    branch: defaultBranch,
    project: project.id,
    mergeAccessLevel: "maintainer",
    pushAccessLevel: "maintainer",
    allowForcePush: false,
    codeOwnerApprovalRequired: false,
  };

  new gitlab.BranchProtection(
    `branch-protection:${resourceName}`,
    branchProtectionArgs,
  );

  projectDefinition.protectedBranches.forEach((protectedBranch) => {
    new gitlab.BranchProtection(
      `branch-protection:${resourceName}:${protectedBranch}`,
      { ...branchProtectionArgs, branch: protectedBranch },
    );
  });

  projectDefinition.pipelineSchedules.forEach((pipelineSchedule) => {
    const pipelineScheduleArgs: gitlab.PipelineScheduleArgs = {
      cron: pipelineSchedule.cron.toString(),
      description: pipelineSchedule.description,
      ref: `refs/heads/${pipelineSchedule.ref}`,
      cronTimezone: pipelineSchedule.cronTimezone,
      project: project.id,
      active: true,
    };
    const pipelineScheduleName = pathFromDisplayName(pipelineSchedule.name);
    new gitlab.PipelineSchedule(
      `pipeline-schedule:${resourceName}:${pipelineScheduleName}`,
      pipelineScheduleArgs,
    );
  });
}

type ParseGroupParameters = {
  resourceNamePrefix: string;
  superGroup: gitlab.Group;
};

function parseGroup(
  groupDefinition: GroupDefinition,
  defaultVisibilityLevel: VisibilityLevel,
  defaultDefaultBranch: string,
  args: ParseGroupParameters | undefined = undefined,
): void {
  const resourceName =
    args === undefined
      ? groupDefinition.slug
      : `${args.resourceNamePrefix}/${groupDefinition.slug}`;
  const visibilityLevel =
    groupDefinition.visibilityLevel === undefined
      ? defaultVisibilityLevel
      : groupDefinition.visibilityLevel;
  let groupArgs: gitlab.GroupArgs = {
    name: groupDefinition.name,
    path: groupDefinition.slug,
    description: groupDefinition.description,
    visibilityLevel: visibilityLevel,
    requestAccessEnabled: false,
    lfsEnabled: false,
  };
  if (args !== undefined) {
    groupArgs = { ...groupArgs, parentId: args.superGroup.id.apply(Number) };
  }
  const gitlabGroup = new gitlab.Group(`group:${resourceName}`, groupArgs);

  groupDefinition.projects.forEach((projectDefinition) => {
    parseProject(
      projectDefinition,
      resourceName,
      gitlabGroup,
      defaultVisibilityLevel,
      defaultDefaultBranch,
    );
  });

  groupDefinition.groups.forEach((groupDefinition) => {
    parseGroup(groupDefinition, defaultVisibilityLevel, defaultDefaultBranch, {
      resourceNamePrefix: resourceName,
      superGroup: gitlabGroup,
    });
  });
}

export const DEFAULT_BRANCH = "main";

export type InfrastructureDefinition = {
  defaultVisibilityLevel?: VisibilityLevel;
  defaultDefaultBranch?: string;
  groups: Array<GroupDefinition>;
};

export function parse({
  defaultVisibilityLevel = VisibilityLevel.PRIVATE,
  defaultDefaultBranch = DEFAULT_BRANCH,
  groups,
}: InfrastructureDefinition): void {
  groups.forEach((groupDefinition) => {
    parseGroup(groupDefinition, defaultVisibilityLevel, defaultDefaultBranch);
  });
}
